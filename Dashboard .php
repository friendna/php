<?php
include('system/connect_db.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        #userList {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #userList td, #userList th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #userList tr:nth-child(even){background-color: #f2f2f2;}

        #userList tr:hover {background-color: #ddd;}

        #userList th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }
        body {
            font-family: Arial;
        }

        * {
            box-sizing: border-box;
        }

        form.example input[type=text] {
            padding: 10px;
            font-size: 17px;
            border: 1px solid grey;
            float: left;
            width: 80%;
            background: #f1f1f1;
        }

        form.example button {
            float: left;
            width: 20%;
            padding: 10px;
            background: #2196F3;
            color: white;
            font-size: 17px;
            border: 1px solid grey;
            border-left: none;
            cursor: pointer;
        }

        form.example button:hover {
            background: #0b7dda;
        }

        form.example::after {
            content: "";
            clear: both;
            display: table;
        }
        .container {
            padding: 16px;
            margin: 30px;
            background-color: white;
        }

    </style>
</head>
<body>
<?php
$sql = "SELECT * FROM users";
$result = $conn->query($sql);
?>

<div class="container">
    <div style="margin-bottom: 20px">
        <form class="example" action="/action_page.php">
            <input type="text" placeholder="Search.." name="search">
            <button type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div>
    <table id="userList">
        <thead>
        <tr>
            <th>ID</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Email</th>
            <th>Password</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        while($row = mysqli_fetch_array($result))
        {    ?>
        <tr>
            <td><?php echo  $row["id"];?></div></td>
    <td><?php echo $row["firstname"];?></div></td>
    <td><?php echo $row["lastname"];?></div></td>
    <td><?php echo $row["email"];?></div></td>
    <td><?php echo $row["psw"];?></div></td>


    </tr>
<?php
}
?>

</tbody>
</table>
</div>
<script src="script.js"></script>
</body>
</html>
