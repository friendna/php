function onFormSubmit(){
  var formData = readFormData();
  insertNewRecord(formData);
}

function readFormData(){
  var formData = {};
  formData["firstname"] = document.getElementById("firstname").value;
  formData["lastname"] = document.getElementById("lastname").value;
  formData["email"] = document.getElementById("email").value;
  formData["psw"] = document.getElementById("psw").value;
  return formData;
}

function insertNewRecord(data){
  var table = document.getElementById("userList").getElementsByTagName('tbody')[0];
  var newRow = table.insertRow(table.length);
  cell1 = newRow.insertCell(0);
  cell1.innerHTML = data.firstname;
  cell2 = newRow.insertCell(1);
  cell2.innerHTML = data.lastname;
  cell3 = newRow.insertCell(2);
  cell3.innerHTML = data.email;
  cell4 = newRow.insertCell(3);
  cell4.innerHTML = data.psw;
  cell4 = newRow.insertCell(4);
  cell4.innerHTML = `<a>Edit</a>
                      <a>Delete</a>`;
}
